/**
 * @File nbody.cu
 *
 * Implementation of the N-Body problem
 *
 * Paralelní programování na GPU (PCG 2020)
 * Projekt c. 1 (cuda)
 * Login: xstepa59
 */

#include <cmath>
#include <cfloat>
#include "nbody.h"


/**
 * CUDA kernel to calculate velocity
 * @param pIn     - Input particles
 * @param pOut    - Output particles
 * @param N       - Number of particles
 * @param dt      - Size of the time step
 * @param dtG     - Precomputed dt * G (just a slight optimization)
 */
__global__ void calculate_velocity(t_particles pIn, t_particles pOut, int N, float dt, float dtG)
{
  int tid = blockIdx.x * blockDim.x + threadIdx.x;

  float4 sumV = {0, 0, 0};
  float4 myPosW = pIn.posW[tid];

  int globalIndex = 0;

  int endIteration;

   // Load only position and weight into shared memory. Velocity of particles is
   // needed only for counting collisions, which does not occur often. Therefore, it would
   // be waste to load all velocities into shared memory all the time.
  extern __shared__ float4 posW[];

  for(int i = threadIdx.x; i < gridDim.x * blockDim.x; i += blockDim.x)
  {
    __syncthreads();

    if(i < N)
      posW[threadIdx.x] = pIn.posW[i];

    __syncthreads();

    if(tid >= N)
      continue;

    endIteration = min(N - globalIndex, blockDim.x);

    for(int indexInActualBlock = 0; indexInActualBlock < endIteration; indexInActualBlock++, globalIndex++)
    {
      float4 otherPosW = posW[indexInActualBlock];
      float dx = otherPosW.x - myPosW.x;
      float dy = otherPosW.y - myPosW.y;
      float dz = otherPosW.z - myPosW.z;

      // Distance squared
      float r2 = dx*dx + dy*dy + dz*dz;

      // To avoid division by zero, set r2 (representing distance^2) to some dummy value when comparing particle with itself.
      if(globalIndex == tid)
        r2 = DUMMY_NONZERO_VALUE;

      // rsqrtf computes 1 / (x^0.5). In this case, 1 / ((r^2)^0.5)) = 1 / r
      float rReciproc = rsqrtf(r2);
      // Brackets are here for forcing parallelism on instruction level
      float F = dtG * otherPosW.w * rReciproc * (rReciproc * rReciproc);

      // 3 cases can happen here:
      //   1) Distant particles are compared - just add (F * distance) to sum of velocities.
      //   2) Particle is compared with itself - distances (dx, dy and dz) are 0, therefore (F * distance) is also 0. Sum of velocities remains unchanged.
      //   3) Close particles are compared - sum of velocities should remain unchanged. However, for optimization reasons, it is better to avoid conditional
      //      addition and to add (F * distance) unconditionaly. To fix the result, (F * distance) must by subtracted again if r < COLLISION_DISTANCE.
      sumV.x += F * dx;
      sumV.y += F * dy;
      sumV.z += F * dz;

      if(r2 <= COLLISION_DISTANCE * COLLISION_DISTANCE && globalIndex != tid)
      {
        // Subtract (F * distance) to fix error caused by unconditionaly adding it.
        sumV.x -= F * dx;
        sumV.y -= F * dy;
        sumV.z -= F * dz;

        float sumOfWeights = pIn.posW[tid].w + otherPosW.w;
        // Prepare denominator - we need to obtain value 1 / (sumOfWeights).
        // rsqrtf computes 1 / (x^0.5). In this case, 1 / ((sumOfWeights * sumOfWeights)^0.5) = 1 / sumOfWeights
        float denominator = rsqrtf(sumOfWeights * sumOfWeights);

        sumV.x += ((pIn.posW[tid].w * pIn.vel[tid].x - otherPosW.w * pIn.vel[tid].x + 2 * otherPosW.w * pIn.vel[globalIndex].x) * denominator) - pIn.vel[tid].x;
        sumV.y += ((pIn.posW[tid].w * pIn.vel[tid].y - otherPosW.w * pIn.vel[tid].y + 2 * otherPosW.w * pIn.vel[globalIndex].y) * denominator) - pIn.vel[tid].y;
        sumV.z += ((pIn.posW[tid].w * pIn.vel[tid].z - otherPosW.w * pIn.vel[tid].z + 2 * otherPosW.w * pIn.vel[globalIndex].z) * denominator) - pIn.vel[tid].z;
      }
    }
  }

  if(tid >= N)
    return;

  sumV.x += pIn.vel[tid].x;
  sumV.y += pIn.vel[tid].y;
  sumV.z += pIn.vel[tid].z;

  pOut.vel[tid] = sumV;

  pOut.posW[tid].x = myPosW.x + sumV.x * dt;
  pOut.posW[tid].y = myPosW.y + sumV.y * dt;
  pOut.posW[tid].z = myPosW.z + sumV.z * dt;

}// end of calculate_velocity
//----------------------------------------------------------------------------------------------------------------------


/**
 * CUDA function to add particle to Center of mass
 * @param com      - center of mass
 * @param p        - particle
 */
__device__ void addParticleToCOM(float4& com, const float4& p)
{
  const float dx = p.x - com.x;
  const float dy = p.y - com.y;
  const float dz = p.z - com.z;

  const float dw = ((p.w + com.w) > 0.0f) ? ( p.w / (p.w + com.w)) : 0.0f;

  com.x += dx * dw;
  com.y += dy * dw;
  com.z += dz * dw;
  com.w += p.w;
}// end of addParticleToCOM
//----------------------------------------------------------------------------------------------------------------------


/**
 * CUDA kernel to update particles
 * @param p       - particles
 * @param comX    - pointer to a center of mass position in X
 * @param comY    - pointer to a center of mass position in Y
 * @param comZ    - pointer to a center of mass position in Z
 * @param comW    - pointer to a center of mass weight
 * @param lock    - pointer to a user-implemented lock
 * @param N       - Number of particles
 */
__global__ void centerOfMass(t_particles p, float* comX, float* comY, float* comZ, float* comW, int* lock, const int N)
{
  int tid = blockIdx.x * blockDim.x + threadIdx.x;

  float4 threadPartialRes{0, 0, 0, 0};
  extern __shared__ float4 blockPartialRes[];

  // Reduce particles in each threads
  for(int i = tid; i < N; i += gridDim.x * blockDim.x)
  {
    float4 particle = p.posW[i];
    addParticleToCOM(threadPartialRes, particle);
  }


  // Warp-synchronous reduction into one result for each warp
  for(int stride = 16; stride > 0; stride >>= 1)
  {
    float4 otherPartialRes;
    __syncwarp();
    otherPartialRes.x = __shfl_down_sync(0xFFFFFFFF, threadPartialRes.x, stride);
    otherPartialRes.y = __shfl_down_sync(0xFFFFFFFF, threadPartialRes.y, stride);
    otherPartialRes.z = __shfl_down_sync(0xFFFFFFFF, threadPartialRes.z, stride);
    otherPartialRes.w = __shfl_down_sync(0xFFFFFFFF, threadPartialRes.w, stride);
    addParticleToCOM(threadPartialRes, otherPartialRes);
  }

  // Index of warp
  int warpId = threadIdx.x >> 5;
  // Index of thread in warp
  float laneId = threadIdx.x & 0b11111;
  if(laneId == 0)
    blockPartialRes[warpId] = threadPartialRes;

  // Reduce partial results in shared memory into one result for each block
  for(unsigned int stride = blockDim.x / 64; stride > 0; stride >>= 1)
  {
    __syncthreads();

    if(threadIdx.x < stride)
      addParticleToCOM(blockPartialRes[threadIdx.x], blockPartialRes[threadIdx.x + stride]);
  }

  // Reduce the per-block results into one global result
  if(threadIdx.x == 0)
  {
    while(atomicCAS(lock, 0, 1) != 0)
      ;

    float4 com = {*comX, *comY, *comZ, *comW};
    addParticleToCOM(com, blockPartialRes[0]);

    *comX = com.x;
    *comY = com.y;
    *comZ = com.z;
    *comW = com.w;

    *lock = 0;
  }
}// end of centerOfMass
//----------------------------------------------------------------------------------------------------------------------

/**
 * CPU implementation of the Center of Mass calculation
 * @param particles - All particles in the system
 * @param N         - Number of particles
 */
__host__ float4 centerOfMassCPU(MemDesc& memDesc)
{
  float4 com = {0 ,0, 0, 0};

  for(int i = 0; i < memDesc.getDataSize(); i++)
  {
    // Calculate the vector on the line connecting points and most recent position of center-of-mass
    const float dx = memDesc.getPosX(i) - com.x;
    const float dy = memDesc.getPosY(i) - com.y;
    const float dz = memDesc.getPosZ(i) - com.z;

    // Calculate weight ratio only if at least one particle isn't massless
    const float dw = ((memDesc.getWeight(i) + com.w) > 0.0f)
                          ? ( memDesc.getWeight(i) / (memDesc.getWeight(i) + com.w)) : 0.0f;

    // Update position and weight of the center-of-mass according to the weight ration and vector
    com.x += dx * dw;
    com.y += dy * dw;
    com.z += dz * dw;
    com.w += memDesc.getWeight(i);
  }
  return com;
}// enf of centerOfMassCPU
//----------------------------------------------------------------------------------------------------------------------
