/**
 * @File nbody.cu
 *
 * Implementation of the N-Body problem
 *
 * Paralelní programování na GPU (PCG 2020)
 * Projekt c. 1 (cuda)
 * Login: xstepa59
 */

#include <cmath>
#include <cfloat>
#include "nbody.h"

/**
 * CUDA kernel to calculate velocity
 * @param pIn     - Input particles
 * @param pOut    - Output particles
 * @param N       - Number of particles
 * @param dt      - Size of the time step
 * @param dtG     - Precomputed dt * G (just a slight optimization)
 */
__global__ void calculate_velocity(t_particles pIn, t_particles pOut, int N, float dt, float dtG)
{
  int tid = blockIdx.x * blockDim.x + threadIdx.x;
  if(tid >= N)
    return;

  float4 sumV = {0, 0, 0};
  float4 myPosW = pIn.posW[tid];

  for(int i = 0; i < N; i++)
  {
    float4 otherPosW = pIn.posW[i];
    float dx = otherPosW.x - myPosW.x;
    float dy = otherPosW.y - myPosW.y;
    float dz = otherPosW.z - myPosW.z;

    // Distance squared
    float r2 = dx*dx + dy*dy + dz*dz;

    // To avoid division by zero, set r2 (representing distance^2) to some dummy value when comparing particle with itself.
    if(i == tid)
      r2 = DUMMY_NONZERO_VALUE;

    // rsqrtf computes 1 / (x^0.5). In this case, 1 / ((r^2)^0.5)) = 1 / r
    float rReciproc = rsqrtf(r2);
    // Brackets are here for forcing parallelism on instruction level
    float F = dtG * otherPosW.w * rReciproc * (rReciproc * rReciproc);

    // 3 cases can happen here:
    //   1) Distant particles are compared - just add (F * distance) to sum of velocities.
    //   2) Particle is compared with itself - distances (dx, dy and dz) are 0, therefore (F * distance) is also 0. Sum of velocities remains unchanged.
    //   3) Close particles are compared - sum of velocities should remain unchanged. However, for optimization reasons, it is better to avoid conditional
    //      addition and to add (F * distance) unconditionaly. To fix the result, (F * distance) must by subtracted again if r < COLLISION_DISTANCE.
    sumV.x += F * dx;
    sumV.y += F * dy;
    sumV.z += F * dz;

    if(r2 <= COLLISION_DISTANCE * COLLISION_DISTANCE && i != tid)
    {
      // Subtract (F * distance) to fix error caused by unconditionaly adding it.
      sumV.x -= F * dx;
      sumV.y -= F * dy;
      sumV.z -= F * dz;

      float sumOfWeights = pIn.posW[tid].w + otherPosW.w;
      // Prepare denominator - we need to obtain value 1 / (sumOfWeights).
      // rsqrtf computes 1 / (x^0.5). In this case, 1 / ((sumOfWeights * sumOfWeights)^0.5) = 1 / sumOfWeights
      float denominator = rsqrtf(sumOfWeights * sumOfWeights);

      sumV.x += ((pIn.posW[tid].w * pIn.vel[tid].x - otherPosW.w * pIn.vel[tid].x + 2 * otherPosW.w * pIn.vel[i].x) * denominator) - pIn.vel[tid].x;
      sumV.y += ((pIn.posW[tid].w * pIn.vel[tid].y - otherPosW.w * pIn.vel[tid].y + 2 * otherPosW.w * pIn.vel[i].y) * denominator) - pIn.vel[tid].y;
      sumV.z += ((pIn.posW[tid].w * pIn.vel[tid].z - otherPosW.w * pIn.vel[tid].z + 2 * otherPosW.w * pIn.vel[i].z) * denominator) - pIn.vel[tid].z;
    }
  }

  sumV.x += pIn.vel[tid].x;
  sumV.y += pIn.vel[tid].y;
  sumV.z += pIn.vel[tid].z;

  pOut.vel[tid] = sumV;

  pOut.posW[tid].x = myPosW.x + sumV.x * dt;
  pOut.posW[tid].y = myPosW.y + sumV.y * dt;
  pOut.posW[tid].z = myPosW.z + sumV.z * dt;

}// end of calculate_velocity
//----------------------------------------------------------------------------------------------------------------------

/**
 * CUDA kernel to update particles
 * @param p       - particles
 * @param comX    - pointer to a center of mass position in X
 * @param comY    - pointer to a center of mass position in Y
 * @param comZ    - pointer to a center of mass position in Z
 * @param comW    - pointer to a center of mass weight
 * @param lock    - pointer to a user-implemented lock
 * @param N       - Number of particles
 */
__global__ void centerOfMass(t_particles p, float* comX, float* comY, float* comZ, float* comW, int* lock, const int N)
{

}// end of centerOfMass
//----------------------------------------------------------------------------------------------------------------------

/**
 * CPU implementation of the Center of Mass calculation
 * @param particles - All particles in the system
 * @param N         - Number of particles
 */
__host__ float4 centerOfMassCPU(MemDesc& memDesc)
{
  float4 com = {0 ,0, 0, 0};

  for(int i = 0; i < memDesc.getDataSize(); i++)
  {
    // Calculate the vector on the line connecting points and most recent position of center-of-mass
    const float dx = memDesc.getPosX(i) - com.x;
    const float dy = memDesc.getPosY(i) - com.y;
    const float dz = memDesc.getPosZ(i) - com.z;

    // Calculate weight ratio only if at least one particle isn't massless
    const float dw = ((memDesc.getWeight(i) + com.w) > 0.0f)
                          ? ( memDesc.getWeight(i) / (memDesc.getWeight(i) + com.w)) : 0.0f;

    // Update position and weight of the center-of-mass according to the weight ration and vector
    com.x += dx * dw;
    com.y += dy * dw;
    com.z += dz * dw;
    com.w += memDesc.getWeight(i);
  }
  return com;
}// enf of centerOfMassCPU
//----------------------------------------------------------------------------------------------------------------------
