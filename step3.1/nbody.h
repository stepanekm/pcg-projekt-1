/**
 * @File nbody.h
 *
 * Header file of your implementation to the N-Body problem
 *
 * Paralelní programování na GPU (PCG 2020)
 * Projekt c. 1 (cuda)
 * Login: xstepa59
 */

#ifndef __NBODY_H__
#define __NBODY_H__

#include <cstdlib>
#include <cstdio>
#include "h5Helper.h"

/* Gravitation constant */
constexpr float G =  6.67384e-11f;
constexpr float COLLISION_DISTANCE = 0.01f;
constexpr float DUMMY_NONZERO_VALUE = 42.f;


/**
 * Particles data structure
 */
typedef struct
{
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//                 FILL IN: Particle data structure optimal for the use on GPU (step 0)                             //
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // Optimal data structure for GPU is float4 because it allows to use vectorized memory access
  // (https://developer.nvidia.com/blog/cuda-pro-tip-increase-performance-with-vectorized-memory-access)
  float4* posW;  // Position and weight
  float4* vel;   // Velocity (only x, y and z are used. w is padding, which is necessary for coalesced loads)
} t_particles;


/**
 * CUDA kernel to calculate velocity
 * @param pIn       - Input particles
 * @param pOut      - Output particles
 * @param N         - Number of particles
 * @param dt        - Size of the time step
 * @param dtG       - Precomputed dt * G (just a slight optimization)
 */
__global__ void calculate_velocity(t_particles  pIn,
                                   t_particles  pOut,
                                   int          N,
                                   float        dt,
                                   float        dtG);

/**
 * CUDA kernel to update particles
 * @param p       - particles
 * @param comX    - pointer to a center of mass position in X
 * @param comY    - pointer to a center of mass position in Y
 * @param comZ    - pointer to a center of mass position in Z
 * @param comW    - pointer to a center of mass weight
 * @param lock    - pointer to a user-implemented lock
 * @param N       - Number of particles
 */
__global__ void centerOfMass(t_particles p,
                             float*      comX,
                             float*      comY,
                             float*      comZ,
                             float*      comW,
                             int*        lock,
                             const int   N);

/**
 * CPU implementation of the Center of Mass calculation
 * @param memDesc - Memory descriptor of particle data on CPU side
 */
float4 centerOfMassCPU(MemDesc& memDesc);

#endif /* __NBODY_H__ */
