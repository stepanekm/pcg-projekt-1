/**
 * @File nbody.cu
 *
 * Implementation of the N-Body problem
 *
 * Paralelní programování na GPU (PCG 2020)
 * Projekt c. 1 (cuda)
 * Login: xstepa59
 */

#include <cmath>
#include <cfloat>
#include "nbody.h"

/**
 * CUDA kernel to calculate gravitation velocity
 * @param p       - particles
 * @param tmp_vel - temp array for velocities
 * @param N       - Number of particles
 * @param dt      - Size of the time step
 */
__global__ void calculate_gravitation_velocity(t_particles p, t_velocities tmp_vel, int N, float dt)
{
  int tid = blockIdx.x * blockDim.x + threadIdx.x;
  if(tid >= N)
    return;

  float4 sumV = {0, 0, 0};

  for(int i = 0; i < N; i++)
  {
    float dx = p.posW[tid].x - p.posW[i].x;
    float dy = p.posW[tid].y - p.posW[i].y;
    float dz = p.posW[tid].z - p.posW[i].z;

    float r = sqrt(dx*dx + dy*dy + dz*dz);

    float F = -G * p.posW[tid].w * p.posW[i].w / (r * r + FLT_MIN);

    float nx = F * dx / (r + FLT_MIN);
    float ny = F * dy / (r + FLT_MIN);
    float nz = F * dz / (r + FLT_MIN);

    float vx = nx * dt / p.posW[tid].w;
    float vy = ny * dt / p.posW[tid].w;
    float vz = nz * dt / p.posW[tid].w;

    sumV.x += (r > COLLISION_DISTANCE) ? vx : 0.0f;
    sumV.y += (r > COLLISION_DISTANCE) ? vy : 0.0f;
    sumV.z += (r > COLLISION_DISTANCE) ? vz : 0.0f;
  }

  tmp_vel.vel[tid] = sumV;

}// end of calculate_gravitation_velocity
//----------------------------------------------------------------------------------------------------------------------

/**
 * CUDA kernel to calculate collision velocity
 * @param p       - particles
 * @param tmp_vel - temp array for velocities
 * @param N       - Number of particles
 * @param dt      - Size of the time step
 */
__global__ void calculate_collision_velocity(t_particles p, t_velocities tmp_vel, int N, float dt)
{
  int tid = blockIdx.x * blockDim.x + threadIdx.x;
  if(tid >= N)
    return;

  float4 sumV = {0, 0, 0, 0};

  for(int i = 0; i < N; i++)
  {
    float dx = p.posW[tid].x - p.posW[i].x;
    float dy = p.posW[tid].y - p.posW[i].y;
    float dz = p.posW[tid].z - p.posW[i].z;

    float r = sqrt(dx*dx + dy*dy + dz*dz);

    float vx = ((p.posW[tid].w * p.vel[tid].x - p.posW[i].w * p.vel[tid].x + 2 * p.posW[i].w * p.vel[i].x) /
            (p.posW[tid].w + p.posW[i].w)) - p.vel[tid].x;
    float vy = ((p.posW[tid].w * p.vel[tid].y - p.posW[i].w * p.vel[tid].y + 2 * p.posW[i].w * p.vel[i].y) /
            (p.posW[tid].w + p.posW[i].w)) - p.vel[tid].y;
    float vz = ((p.posW[tid].w * p.vel[tid].z - p.posW[i].w * p.vel[tid].z + 2 * p.posW[i].w * p.vel[i].z) /
            (p.posW[tid].w + p.posW[i].w)) - p.vel[tid].z;

    if (i != tid && r < COLLISION_DISTANCE) {
        sumV.x += vx;
        sumV.y += vy;
        sumV.z += vz;
    }
  }

  tmp_vel.vel[tid].x += sumV.x;
  tmp_vel.vel[tid].y += sumV.y;
  tmp_vel.vel[tid].z += sumV.z;

}// end of calculate_collision_velocity
//----------------------------------------------------------------------------------------------------------------------

/**
 * CUDA kernel to update particles
 * @param p       - particles
 * @param tmp_vel - temp array for velocities
 * @param N       - Number of particles
 * @param dt      - Size of the time step
 */
__global__ void update_particle(t_particles p, t_velocities tmp_vel, int N, float dt)
{
  int tid = blockIdx.x * blockDim.x + threadIdx.x;
  if(tid >= N)
    return;

  p.vel[tid].x += tmp_vel.vel[tid].x;
  p.vel[tid].y += tmp_vel.vel[tid].y;
  p.vel[tid].z += tmp_vel.vel[tid].z;

  p.posW[tid].x += p.vel[tid].x * dt;
  p.posW[tid].y += p.vel[tid].y * dt;
  p.posW[tid].z += p.vel[tid].z * dt;

}// end of update_particle
//----------------------------------------------------------------------------------------------------------------------

/**
 * CUDA kernel to update particles
 * @param p       - particles
 * @param comX    - pointer to a center of mass position in X
 * @param comY    - pointer to a center of mass position in Y
 * @param comZ    - pointer to a center of mass position in Z
 * @param comW    - pointer to a center of mass weight
 * @param lock    - pointer to a user-implemented lock
 * @param N       - Number of particles
 */
__global__ void centerOfMass(t_particles p, float* comX, float* comY, float* comZ, float* comW, int* lock, const int N)
{

}// end of centerOfMass
//----------------------------------------------------------------------------------------------------------------------

/**
 * CPU implementation of the Center of Mass calculation
 * @param particles - All particles in the system
 * @param N         - Number of particles
 */
__host__ float4 centerOfMassCPU(MemDesc& memDesc)
{
  float4 com = {0 ,0, 0, 0};

  for(int i = 0; i < memDesc.getDataSize(); i++)
  {
    // Calculate the vector on the line connecting points and most recent position of center-of-mass
    const float dx = memDesc.getPosX(i) - com.x;
    const float dy = memDesc.getPosY(i) - com.y;
    const float dz = memDesc.getPosZ(i) - com.z;

    // Calculate weight ratio only if at least one particle isn't massless
    const float dw = ((memDesc.getWeight(i) + com.w) > 0.0f)
                          ? ( memDesc.getWeight(i) / (memDesc.getWeight(i) + com.w)) : 0.0f;

    // Update position and weight of the center-of-mass according to the weight ration and vector
    com.x += dx * dw;
    com.y += dy * dw;
    com.z += dz * dw;
    com.w += memDesc.getWeight(i);
  }
  return com;
}// enf of centerOfMassCPU
//----------------------------------------------------------------------------------------------------------------------
